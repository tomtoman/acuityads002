
var teamDescription = "Digital Advertising Is Known For Two Things: Its Potential And Its Shortcomings. This Is Not Lost On Us. It’s What Drives Us. Realizing The Tremendous Advantages Of Our Business While Eradicating The Threats Is Nothing Short Of An Obsession. At Acuity, We Strive To Systematically And Mathematically Take The Risk, Mystery And Waste Out Of The Media Buying Process To Ensure – And Even Prove – That Every Advertising Dollar Can Be Spent Wisely And Well.";

export default teamDescription;
