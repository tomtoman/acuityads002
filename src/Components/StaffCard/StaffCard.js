
import React from 'react'
import './StaffCard.css'

class Card extends React.Component{

  buttonListener(){
      alert('Button Listener Needs To Be Implemented');
  }

  render(){
    return (

      <div className='col-sm-12 col-lg-6 acuityads-cards' >

        <div className='card'>

          <div className='row h-100'>

            <div className='col-4 col-sm-4 col-lg-4 d-flex flex-column justify-content-center' >
              <img src={this.props.imgUrl} alt='Profile' className='img-fluid' />
            </div>

            <div className='col-8 col-sm-8 col-lg-8 h-100'>

              <div className='acuityads-cards-name d-flex align-items-end flex-row'>
                <div className='font-weight-bold'>
                  {this.props.name}
                </div>
              </div>

              <div className='acuityads-cards-title d-flex align-items-start flex-row'>
                <div className='font-weight-bold'>
                  {this.props.title}
                </div>
              </div>

              <hr className='border border-warning'/>

              <div className='acuityads-cards-bio'>
                <p>
                  {this.props.bio}
                </p>
              </div>

              <div className='acuityads-cards-button d-flex align-items-center'>
                <button className='btn btn-primary' onClick={this.buttonListener}>Read More</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Card;
