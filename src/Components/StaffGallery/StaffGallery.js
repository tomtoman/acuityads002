
import React from 'react'
import StaffCard from '../StaffCard/StaffCard'

import staffs from './../../ApiData/Staffs'

console.log(staffs);

const Gallery = () => (

  <div className='col'>
    <div className='row'>
      {staffs.map((staff)=>{
        return <StaffCard name={staff.name} title={staff.title} bio={staff.bio} imgUrl={staff.imgUrl} ></StaffCard>
      })}
    </div>
  </div>
)

export default Gallery;
