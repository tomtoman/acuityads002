
import React from 'react'
import TeamDescription from './../../ApiData/TeamDescription';

const Team = () => (

  <div className='col'>
    <div className='row m-1 m-lg-3'>
      <div className='col text-center'>
        <h1>Team</h1>
      </div>
    </div>
    <div className='row m-1 m-lg-3'>
      <div className='col text-center'>
        <p className='font-weight-light'>{TeamDescription}</p>
      </div>
    </div>
  </div>
)

export default Team;
