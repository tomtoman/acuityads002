import React from 'react';
import './App.css';

import StaffGallery from './Components/StaffGallery/StaffGallery';
import TeamHeader from './Components/TeamHeader/TeamHeader';
import TeamInfo from './Components/TeamInfo/TeamInfo';

function App() {
  return (

    <div>

      <div className='navbar navbar-light bg-light'>
        <TeamHeader />
      </div>

      <div className='container'>
        <div className='row'>
          <TeamInfo />
        </div>
        <div className='row'>
          <StaffGallery />
        </div>
      </div>
    </div>
  );
}

export default App;
